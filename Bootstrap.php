<?php

namespace sim4nix\news;

use yii\base\BootstrapInterface;

/**
 * Blogs module bootstrap class.
 */
class Bootstrap implements BootstrapInterface
{
	/**
	 * @inheritdoc
	 */
	public function bootstrap($app)
	{
		// Add module I18N category.
		if (!isset($app->i18n->translations['sim4nix/news']) && !isset($app->i18n->translations['sim4nix/*'])) {
			$app->i18n->translations['sim4nix/news'] = [
				'class'            => 'yii\i18n\PhpMessageSource',
				'basePath'         => '@sim4nix/news/messages',
				'forceTranslation' => true,
				'fileMap'          => [
					'sim4nix/news' => 'news.php',
				]
			];
		}
	}
}
