<?php

namespace sim4nix\news;
use sim4nix\components\BaseModule;
use sim4nix\helpers\App;
use Yii;

class Module extends BaseModule
{
	public function getName()
	{
		return static::t('News');
	}
}
