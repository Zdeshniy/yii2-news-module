Yii2 news module.
========================
This module provide a news managing system for Yii2 application.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require sim4nix/yii2-news-module "*"
```

or add

```
"sim4nix/yii2-news-module": "*"
```

to the require section of your `composer.json` file.

Configuration
=============

- Add module to config section:

```
'modules' => [
    'news' => [
        'class' => 'sim4nix\news\Module'
    ]
]
```

- Run migrations:

```
php yii migrate --migrationPath=@sim4nix/news/migrations
```

- Set alias in config file:

```
Yii::setAlias('upload', dirname(dirname(__DIR__)) . '/frontend/web/upload');
```

- Add to params section for uploading files:

```
	'news'                        => [
		'thumbs' => [
			'thumb' => ['width' => 100, 'height' => 100, 'method' => 'resize'],
		]
	],
```
