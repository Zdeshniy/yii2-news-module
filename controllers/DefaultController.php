<?php

namespace sim4nix\news\controllers;

use sim4nix\components\controllers\BackendController;
use sim4nix\file\FileInput;
use sim4nix\news\models\search\Search;
use sim4nix\news\Module;
use sim4nix\widgets\editors\redactor\Redactor;
use Yii;
use sim4nix\news\models\ar\News;
use yii\helpers\ArrayHelper;

/**
 * DefaultController implements the CRUD actions for News model.
 */
class DefaultController extends BackendController
{
	public $modelName = 'sim4nix\news\models\ar\News';

	public function actions()
	{
		return ArrayHelper::merge(
			parent::actions(),
			[
				'index' => [
					'modelName' => Search::className(),
					'title' => Module::t('News')
				],
				'create' => [
					'modelName' => function() {
						$new = new News();
						$new->setAttribute('created', time());
						return $new;
					},
				],
				'toggle' => [
					'class' => 'sim4nix\components\actions\ToggleAction',
					'modelName' => $this->modelName,
					// Uncomment to enable flash messages
					//'setFlash' => true,
				]
			],
			Redactor::getActions(),
			FileInput::getActions($this->modelName)
		);
	}
}
