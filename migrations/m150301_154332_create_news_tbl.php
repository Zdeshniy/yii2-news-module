<?php

use sim4nix\components\BaseMigration;

class m150301_154332_create_news_tbl extends BaseMigration
{
	const TABLE = 'news';

	public function up()
	{
		try {
			$this->createTable(
				static::TABLE,
				[
					'id'          => $this->primaryKey(),
					'created_at'  => $this->integer()->notNull(),
					'updated_at'  => $this->integer()->notNull(),
					'url'         => $this->string()->notNull(),
					'title'       => $this->string()->notNull(),
					'announce' 	  => $this->text(),
					'description' => $this->text(),
					'created' 	  => $this->integer(),
					'image' 	  => $this->string(),
					'active'      => $this->smallInteger()->notNull()->defaultValue(1),
				]
			);
		} catch (\yii\base\Exception $e) {
			$this->dropTable(static::TABLE);
			throw $e;
		}
	}

	public function down()
	{
		$this->dropTable(static::TABLE);
	}
}
