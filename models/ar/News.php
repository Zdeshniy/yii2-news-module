<?php

namespace sim4nix\news\models\ar;

use sim4nix\news\Module;
use sim4nix\widgets\editors\redactor\Redactor;
use Yii;
use sim4nix\helpers\App;
use yii\behaviors\TimestampBehavior;
use sim4nix\uploadfile\UploadImageBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "news".
 *
 * @method string getThumbUploadUrl($attribute, $profile = 'thumb')
 * @method static NewsQuery find()
 */
class News extends \sim4nix\news\models\ar\base\News
{
	/**
	 * @inheritdoc
	 */
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if ($this->created) {
				$this->created = Yii::$app->formatter->asTimestamp($this->created);
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['created_at', 'updated_at', 'active'], 'integer'],
			[['url', 'title', 'created', 'announce', 'description'], 'required'],
			[['announce', 'description'], 'string'],
			[['url', 'title'], 'string', 'max' => 255],
			[['image'], 'safe'],
			[['image'], 'file', 'extensions' => ['png', 'jpg', 'gif']],
		];
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		$params = App::params(static::tableName());

		$behaviors = [
			'timestampBehavior' => [
				'class' => TimestampBehavior::className(),
			],
			'image' => [
				'class' => UploadImageBehavior::className(),
				'attribute' => 'image',
				'scenarios' => ['admin-create', 'admin-update'],
				'thumbs'                => ArrayHelper::getValue($params, 'thumbs'),
				'placeholder'           => ArrayHelper::getValue($params, 'placeholder'),
				'createThumbsOnRequest' => ArrayHelper::getValue($params, 'createThumbsOnRequest', false),
				'createThumbsOnSave'    => ArrayHelper::getValue($params, 'createThumbsOnSave', true),
				'thumbPath'             => ArrayHelper::getValue($params, 'thumbPath'),
				'thumbUrl'              => ArrayHelper::getValue($params, 'thumbUrl'),
			],
		];

		if (Yii::$app->isMultiLanguage) {
			$behaviors[] = [
				'class'                 => 'sim4nix\i18n\behaviors\TranslateableBehavior',
				'translationAttributes' => [
					'title',
					'announce'    => [
						'widget'  => Redactor::className(),
						'options' => [
							'data-fieldtype' => 'redactor'
						]
					],
					'description' => [
						'widget'  => Redactor::className(),
						'options' => [
							'data-fieldType' => 'redactor'
						]
					],
				]
			];
		}

		return $behaviors;
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		$scenarios = parent::scenarios();
		$scenarios['admin-create'] = [
			'title',
			'url',
			'created_at',
			'description',
			'announce',
			'created',
			'image',
			'active',
		];
		$scenarios['admin-update'] = [
			'title',
			'url',
			'updated_at',
			'description',
			'announce',
			'created',
			'image',
			'active',
		];

		return $scenarios;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'          => Module::t('ID'),
			'created_at'  => Module::t('Created At'),
			'updated_at'  => Module::t('Updated At'),
			'url'         => Module::t('Url'),
			'title'       => Module::t('Title'),
			'description' => Module::t('Description'),
			'announce' 	  => Module::t('Announce'),
			'created'     => Module::t('Created'),
			'image'       => Module::t('Image'),
			'active'      => Module::t('Active'),
		];
	}
}
