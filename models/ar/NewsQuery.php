<?php

namespace sim4nix\news\models\ar;

use yii\db\ActiveQuery;

class NewsQuery extends ActiveQuery
{
	/**
	 * @param int $active
	 *
	 * @return static
	 */
	public function active($active = 1)
	{
		return $this->andWhere(['active' => $active]);
	}

	/**
	 * @param string $order
	 *
	 * @return static
	 */
	public function order($order = 'created DESC')
	{
		return $this->orderBy($order);
	}

	/**
	 * @return array|\yii\db\ActiveRecord[]
	 */
	public static function findAllActive()
	{
		$query = News::find();
		return $query->active()->order()->all();
	}

	/**
	 * @param $limit
	 *
	 * @return array|\yii\db\ActiveRecord[]
	 */
	public static function findAllActiveByLimit($limit)
	{
		$query = News::find();
		return $query->active()->order()->limit($limit)->all();
	}
}