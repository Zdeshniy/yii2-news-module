<?php

namespace sim4nix\news\models\ar\base;

use sim4nix\components\db\activeRecord\DbActiveRecord;
use sim4nix\components\db\attributes\ActiveAttribute;
use sim4nix\components\db\attributes\AnnounceAttribute;
use sim4nix\components\db\attributes\DescriptionAttribute;
use sim4nix\components\db\attributes\ImageAttribute;
use sim4nix\components\db\attributes\TitleAttribute;
use sim4nix\components\db\attributes\UrlAttribute;
use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $url
 * @property string $title
 * @property string $announce
 * @property string $description
 * @property integer $created
 * @property string $image
 * @property integer $active
 */
class News extends DbActiveRecord
{
	use AnnounceAttribute;
	use DescriptionAttribute;
	use UrlAttribute;
	use ImageAttribute;
	use TitleAttribute;
	use ActiveAttribute;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created', 'active'], 'integer'],
            [['url', 'title'], 'required'],
            [['announce', 'description'], 'string'],
            [['url', 'title', 'image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => App::t('ID'),
            'created_at' => App::t('Created At'),
            'updated_at' => App::t('Updated At'),
            'url' => App::t('Url'),
            'title' => App::t('Title'),
            'announce' => App::t('Announce'),
            'description' => App::t('Description'),
            'created' => App::t('Created'),
            'image' => App::t('Image'),
            'active' => App::t('Active'),
        ];
    }
}
