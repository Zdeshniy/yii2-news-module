<?php

namespace sim4nix\news\models\search;

use kartik\grid\ActionColumn;
use kartik\grid\CheckboxColumn;
use sim4nix\grid\ToggleColumn;
use sim4nix\helpers\App;
use Yii;
use yii\data\ActiveDataProvider;
use sim4nix\news\models\ar\News;
use yii\helpers\Html;

/**
 * Search represents the model behind the search form about `\sim4nix\news\models\ar\News`.
 */
class Search extends News
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'created_at', 'updated_at', 'created', 'active'], 'integer'],
			[['url', 'title', 'description', 'image'], 'safe'],
		];
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = News::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => [
				'defaultOrder' => [
					'created'       => SORT_DESC,
				]
			]
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'created_at' => $this->created_at,
			'updated_at' => $this->updated_at,
			'created' => $this->created,
			'active' => $this->active,
		]);

		$query->andFilterWhere(['like', 'url', $this->url])
			->andFilterWhere(['like', 'title', $this->title])
			->andFilterWhere(['like', 'description', $this->description])
			->andFilterWhere(['like', 'image', $this->image]);

		return $dataProvider;
	}

	/**
	 * @return array
	 */
	public function getColumns()
	{
		return [
			[
				'class'         => CheckboxColumn::className(),
				'width'         => '36px',
				'headerOptions' => ['class' => 'kartik-sheet-style'],
			],
			'id',
			[
				'attribute' => 'created',
				'format'    => 'raw',
				'value'     => function (News $data) {
					return App::formatter()->asDatetime($data->created, 'yyyy-MM-dd');
				},
			],
			[
				'attribute' => 'title',
				'format'    => 'raw',
				'value'     => function ($data) {
					return Html::a($data->title, \yii\helpers\Url::toRoute(['update', 'id' => $data->primaryKey]));
				},
			],
			[
				'attribute' => 'image',
				'format'    => 'raw',
				'value'     => function ($data) {
					return Html::a(
						Html::img($data->getThumbUploadUrl('image', 'thumb'), ['class' => 'img-thumbnail']),
						\yii\helpers\Url::toRoute(['update', 'id' => $data->primaryKey]));
				},
			],
			'url',
			[
				'class' => ToggleColumn::className(),
				'attribute' => 'active',
				'filter'              => ["нет", "да"],
			],

			[
				'class' => ActionColumn::className(),
				'template' => '{update} {delete}'
			],
		];
	}
}
