<?php

use yii\helpers\Html;
use sim4nix\i18n\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model sim4nix\news\models\ar\News */
/* @var $form \sim4nix\i18n\widgets\ActiveForm */
/* @var $redirectUrl string */

sim4nix\components\bundles\JsTranslitAsset::register($this);
?>

<div>
	<?= ActiveForm::widget([
		'enableAjaxValidation' => true,
		'options' => [
			'enctype' => 'multipart/form-data'
		],
		'model' => $model,
		'bodyHandler' => function(ActiveForm $form) {
			echo $form->fieldM('created')->widget(
				\yii\jui\DatePicker::className(),
				[
					'options'       => [
						'class' => 'form-control'
					],
					'dateFormat'    => 'yyyy-MM-dd',
					'clientOptions' => [
						'dateFormat'  => 'dd.mm.yy',
						'changeMonth' => true,
						'changeYear'  => true
					]
				]
			);;
			echo $form->fieldM('title')->textInput(['maxlength' => 255, 'data-chputitle' => '1']);
			echo $form->fieldM('url')->textInput(['maxlength' => 255, 'data-chpu' => '1']);
			echo $form->fieldM('announce')->widget(\sim4nix\widgets\editors\redactor\Redactor::className());
			echo $form->fieldM('description')->widget(\sim4nix\widgets\editors\redactor\Redactor::className());
			echo $form->fieldM('image')->widget(\sim4nix\file\FileInput::className());;
			echo $form->fieldM('active')->widget(\kartik\switchinput\SwitchInput::className());
		},
		'footerHandler' => function() use ($redirectUrl) {
			echo $this->render('@sim4nix/components/actions/views/formBtns', ['redirectUrl' => $redirectUrl]);
		}
	]) ?>

</div>
