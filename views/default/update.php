<?php

use yii\helpers\Html;
use sim4nix\news\Module;

/* @var $this yii\web\View */
/* @var $model sim4nix\news\models\ar\News */

$this->title = Module::t('Update news: {name}', 'news', ['name' => $model->title]);
$this->params['breadcrumbs'][] = ['label' => Module::t('News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div>
    <?= $this->render('_form', [
        'model' => $model,
		'redirectUrl' => $redirectUrl,
	]) ?>
</div>
